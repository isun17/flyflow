package cc.flyflow.core.controller;

import cc.flyflow.common.dto.*;
import cc.flyflow.core.service.ITaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 任务控制器
 */
@Api(value = "task", tags = {"任务控制器"})
@RestController
@Slf4j
@RequestMapping("task")
public class TaskController {

    @Autowired
    private ITaskService taskService;


    /**
     * 查询任务变量
     *
     * @param paramDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "VariableQueryParamDto", name = "paramDto", value = "", required = true)
    })
    @ApiOperation(value = "查询任务变量", notes = "查询任务变量", httpMethod = "POST")
    @PostMapping("queryTaskVariables")
    public R<Map<String, Object>> queryTaskVariables(@RequestBody VariableQueryParamDto paramDto) {
        return taskService.queryTaskVariables(paramDto);

    }

    /**
     * 查询任务评论
     *
     * @param paramDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "VariableQueryParamDto", name = "paramDto", value = "", required = true)
    })
    @ApiOperation(value = "查询任务评论", notes = "查询任务评论", httpMethod = "POST")
    @PostMapping("queryTaskComments")
    public R<List<SimpleApproveDescDto>> queryTaskComments(@RequestBody VariableQueryParamDto paramDto) {
        return taskService.queryTaskComments(paramDto);
    }



    /**
     * 查询任务
     *
     * @param taskId
     * @return
     */
    @ApiOperation(value = "查询任务", notes = "查询任务", httpMethod = "GET")
    @GetMapping("queryTask")
    public R<TaskResultDto> queryTask(String taskId, String userId) {
        return taskService.queryTask(taskId, userId);
    }


    /**
     * 完成任务
     *
     * @param taskParamDto
     * @return
     */
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", dataType = "TaskParamDto", name = "taskParamDto", value = "", required = true)
    })
    @ApiOperation(value = "完成任务", notes = "完成任务", httpMethod = "POST")
    @Transactional
    @PostMapping("/complete")
    public R complete(@RequestBody TaskParamDto taskParamDto) {
        return taskService.complete(taskParamDto);
    }



}
