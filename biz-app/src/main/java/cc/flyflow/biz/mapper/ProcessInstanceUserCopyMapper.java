package cc.flyflow.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cc.flyflow.biz.entity.ProcessInstanceUserCopy;

/**
 * <p>
 * 流程抄送数据 Mapper 接口
 * </p>
 *
 * @author Vincent
 * @since 2023-05-20
 */
public interface ProcessInstanceUserCopyMapper extends BaseMapper<ProcessInstanceUserCopy> {

}
