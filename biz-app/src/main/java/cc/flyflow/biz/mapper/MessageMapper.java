package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 通知消息 Mapper 接口
 * </p>
 *
 * @author xiaoge
 * @since 2023-07-25
 */
public interface MessageMapper extends BaseMapper<Message> {

}
