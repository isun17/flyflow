package cc.flyflow.biz.mapper;

import cc.flyflow.biz.entity.ProcessForm;
import com.github.yulichang.base.MPJBaseMapper;

/**
 *
 * @author xiaoge
 * @since 2023-05-05
 */
public interface ProcessFormMapper extends MPJBaseMapper<ProcessForm> {



}
