package cc.flyflow.biz.service;

import cc.flyflow.biz.entity.ProcessStarter;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 流程发起人 服务类
 * </p>
 *
 * @author Vincent
 * @since 2023-05-30
 */
public interface IProcessStarterService extends IService<ProcessStarter> {

}
