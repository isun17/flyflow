package cc.flyflow.biz.service;

import cc.flyflow.biz.entity.ProcessInstanceExecution;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zhj
 * @version 1.0
 * @description: TODO
 * @date 2024/4/2 14:45
 */
public interface IProcessInstanceExecutionService extends IService<ProcessInstanceExecution> {
}
